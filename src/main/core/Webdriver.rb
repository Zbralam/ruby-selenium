require 'selenium-webdriver'

class Webdriver

  # this Keyword global variable sets the <keyword> all around the application
  $keyword = "helloWorld"

  # initialize driver for firefox
  def driver_initialize
   puts "Step 1: Opening browser"
   options = Selenium::WebDriver::Chrome::Options.new()
   $web_driver = Selenium::WebDriver.for(:chrome, options: options)
   $wait = Selenium::WebDriver::Wait.new(:timeout => 30) # seconds
  end

  # this function will clear all browser cookies
  def clearing_all_browser_cookies
    puts "Step 2: Deleting All Cookies"
    $web_driver.manage.delete_all_cookies if($web_driver.manage.respond_to? :delete_all_cookies)
    puts "All Cookies are deleted"
  end

  # this funtion will redirect website to upwork url
  def redirect_to_website
   puts "Step 3: Re-directed to upwork website"
   $web_driver.get("https://www.upwork.com")
   $web_driver.manage.window.maximize
   puts "Step 3: Redirected Successfully"
   sleep 5
  end

  # quit driver and close firefox browser
 def driver_quit
   puts "Step 12: Closing the browser"
   $web_driver.quit
 end

end
