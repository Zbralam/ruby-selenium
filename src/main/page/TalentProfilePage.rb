require 'selenium-webdriver'

class TalentProfilePage

  TALENT_NAME = '.up-card-section h1'
  TALENT_TITLE = '.col section:nth-child(1) h2'
  TALENT_DESCRIPTION = "section section:nth-child(1) > .row div div  span"
  TALENT_SKILLS = "section.row ul > li > span > a"

  def get_talent_name
    $wait.until { $web_driver.find_element(:css => TALENT_NAME) }
  end

  def get_talent_title
    $wait.until { $web_driver.find_element(:css => TALENT_TITLE) }
  end

  def get_talent_description
    $wait.until { $web_driver.find_element(:css => TALENT_DESCRIPTION) }
  end

  def get_talent_skills
    $wait.until { $web_driver.find_element(:css => TALENT_SKILLS) }
  end

end
