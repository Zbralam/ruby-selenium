require 'selenium-webdriver'

class HomePage

  DROPDOWN_BUTTON = "button:nth-child(3) > span > svg"
  SEARCH_BUTTON = "form button:nth-child(2) svg"
  SEARCH_INPUT_FIELD = "form input:nth-child(2)"
  TALENT_DROPDOWN = ".nav-dropdown-search li:nth-child(1)"
  CANCEL_BUTTON = ".buttons button:nth-child(2)"


  def get_dropdown_button
    $wait.until { $web_driver.find_element(:css => DROPDOWN_BUTTON) }
  end

  def get_search_button
    $wait.until { $web_driver.find_element(:css => SEARCH_BUTTON) }
  end

  def get_search_input_field
    $wait.until { $web_driver.find_element(:css => SEARCH_INPUT_FIELD) }
  end

  def get_talent_dropdown
    $wait.until { $web_driver.find_element(:css => TALENT_DROPDOWN) }
  end

  def get_cancel_button
    $wait.until { $web_driver.find_element(:css => CANCEL_BUTTON) }

  end

end
