require 'selenium-webdriver'

class SearchResult

  # this function fetch all data from the page
  def fetching_talent_data
    puts "Step 6: Fetching all freelancer data from the page"
    $web_driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
    sleep 5
    $web_driver.execute_script("window.scrollTo(document.body.scrollHeight, 0)")
    $talents_card = $web_driver.find_elements(css: "div.up-card-hover")
    $talents_card_data = $talents_card.collect{|t|
      title = t.find_element(css: ".freelancer-title strong").text
      description = t.find_elements(css: "div.up-line-clamp-v2").last.text
      name = t.find_elements(css: "div.identity-name").last.text
      {
          title: title,
          description: description,
          name: name
      }
    }
    puts "All data fetch successfully"
  end

  #this function campare freelancer with keyword provided.
  def compare_talent_with_keyword
    puts "Step 7: Comparing Freelancer with Keyword value"
    $talents_card_data.each_with_index do |f, i|
      puts "Talent  #{i} (#{f[:name]})"
      puts "Talent #{i} (#{f[:title]})"
      puts "Talent #{i} (#{f[:description]})"
      fields.each do |fi|
        if f[fi].to_s.match(/#{$keyword}/i)
          puts "\t#{fi} matches #{$keyword}"
        else
          puts "\t#{fi} does not match #{$keyword}"
        end
      end
    end
    puts "All results printed successfully"
  end

  def fields
    [:name ,:description, :title]
  end

end
