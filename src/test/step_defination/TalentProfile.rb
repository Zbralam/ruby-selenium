require 'selenium-webdriver'
require './src/main/page/TalentProfilePage'

class TalentProfile

  # this function generate random freelancer
  def generate_random_talent
    puts "Step 8: Generating random freelancer"
    $index = rand( $talents_card.length)
    $talent = $talents_card[$index]
    $talent.click
    puts "Random freelancer Clicked"
    sleep 20
  end

  def profile_tile123
    freelance_profile_page = FreelancerProfilePage.new
    freelance_profile_page.get_freelancer_title_name.last
  end

  #this function will click on the freelancer
  def click_on_freelancer_profile
    puts "Step 9: Click on Freelancer Profile"
    $freelancer.find_elements(css: "a.freelancer-tile-name").last.click
    puts "Freelancer Profile clicked successfully"

  end

  # def get_profile
  #   freelance_profile_page = FreelancerProfilePage.new
  #   @profile=freelance_profile_page.get_profile_header
  # end

   #this function will fetch all profile data of selected freelancer
  def fetch_profile_data
    puts "Step 10: Extracting Talent profile data"
    talent_profile_page = TalentProfilePage.new
    name = talent_profile_page.get_talent_name.text
    title = talent_profile_page.get_talent_title.text
    description = talent_profile_page.get_talent_description.text
    @profile_data = {
        name: name,
        title: title,
        description: description,
    }

    puts "Profile data extracted successfully"
  end

  #this function will compare freelancer data
  def compare_profile_data
    puts "Step 11: Comparing Freelancer Profile data"
    parent = $talents_card_data[$index]
    child = @profile_data
    puts "for freelancer #{parent[:name]}, comparing results page fields with internal page fields:"
    [:name, :title, :description].each do |field|
      verify_details(parent, child, field, parent[:name])
    end
    puts "All results are printed successfully"

  end

  #this function verify details
  def verify_details(parent, child, field, name)
    if parent[field] == child[field]
      puts "\t#{field.to_s} matches for #{name} on both results page and internal page"
    else
      puts "\t#{field.to_s} does not match for #{name} on results page vs internal page"
    end
  end

end
