require 'selenium-webdriver'
require './src/main/page/HomePage'

class Home

  # this function will search the keyword provide in the global var $keyword
  def search_keyword_talent
    puts "Step 4: Searching keyword"
    sleep 3
    homepage = HomePage.new
    homepage.get_dropdown_button.click
    homepage.get_talent_dropdown.click
    homepage.get_search_input_field.send_keys($keyword)
    homepage.get_search_button.click
    sleep 60
    begin
        homepage.get_cancel_button.click
    rescue
      puts 'cancel button not found'
    ensure
      puts 'moving forward'
    end

  end

  # this funtion will click on magnifying glass button (search button)
  def click_search_icon
    puts "Step 5: Click on magnifying glass button"
    homepage = HomePage.new
    homepage.get_search_icon.click
    puts "Keyword searched successfully"
  end


end