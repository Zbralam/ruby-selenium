require 'selenium-webdriver'
require './src/main/core/Webdriver'
require './src/test/step_defination/Home'
require './src/test/step_defination/SearchResult'
require './src/test/step_defination/TalentProfile'

class ScenarioTest


  def test_all_scenario

    # creating core_webdriver object
    web_driver = Webdriver.new

    # calling function of webdriver class
    web_driver.driver_initialize
    web_driver.clearing_all_browser_cookies
    web_driver.redirect_to_website

    # creating step definition home object to use
    home = Home.new

    # calling function to use home functions
    home.search_keyword_talent
    # creating search result object
    search_result = SearchResult.new
    # calling their function
    search_result.fetching_talent_data
    search_result.compare_talent_with_keyword

    # creating talent profile object
    talent_profile = TalentProfile.new

    # calling their function to  click on random freelancer
    talent_profile.generate_random_talent
    # fetching profile details
    talent_profile.fetch_profile_data
    # compare profile information
    talent_profile.compare_profile_data

    #Finally call driver quit function to quit the browser
    web_driver.driver_quit

  end


  test = ScenarioTest.new
  test.test_all_scenario

end